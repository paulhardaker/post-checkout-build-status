#!/usr/bin/env ruby

# post-checkout hook for determining the build status of the checked out ref from your Bamboo server.
#
# Requires Ruby 1.9.3+ 

require 'yaml'
require 'json'
require 'net/https'
require 'uri'

# utility for correctly pluralizing quantities
def pluralize count, single, multiple
  count == 1 ? single : multiple
end

# parse args supplied by git
ref = ARGV[1]       # ref being checked out
isBranch = ARGV[2]  # 0 = file checkout, 1 = branch checkout

# we only care about branch checkouts 
if isBranch == "1"
  # initialise build status counts
  failed = successful = in_progress = 0
  
  # loop through each configured Stash server, retrieving build statuses for the checked out commit and 
  # counting the number of failed, successful and in progress builds
  hookDir = File.expand_path File.dirname(__FILE__)
  configPath = hookDir + "/bamboo-config.yml"
  raise "No bamboo-config.yml found." unless File.exists? configPath
  config = YAML.load_file(configPath)
  raise "bamboo-config.yml file is incomplete: username, password & url are required" unless config['url'] and config['username'] and config['password']
    
  # normalize base url
  baseUrl = config['url']
  # assume https if no scheme spcified
  if not baseUrl.start_with? "http"
    baseUrl = "https://#{baseUrl}"
  end
  # strip trailing slashes
  while baseUrl.end_with? "/"
    baseUrl = baseUrl[0..-2]
  end

  # prepare a request to hit the build status REST end-point
  build_status_resource = "#{baseUrl}/rest/api/latest/result/byChangeset"
  uri = URI.parse("#{build_status_resource}/#{ref}")
  req = Net::HTTP::Get.new(uri.to_s, initheader = {'Content-Type' => 'application/json', 'Accept' => 'application/json'})
  req.basic_auth config['username'], config['password']
  http = Net::HTTP.new(uri.host, uri.port)
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  http.use_ssl = uri.scheme.eql?("https")

  # execute the request
  response = http.start {|http| http.request(req)}
      
  if not response.is_a? Net::HTTPOK
    puts 'An unknown error occurred while querying Bamboo for build results.'    
    exit    
  else  
    # if the request succeeded, count the statuses from the response
    body = JSON.parse(response.body)        
    body['results']['result'].collect { |result|
      case result['state']
      when "Failed" 
          failed += 1
      when "Successful"
          successful += 1
      when "Unknown"
          if result['lifeCycleState'] == "InProgress"
            in_progress += 1
          end
      end
    }
  end    

  # display a short message describing the build status for the checked out commit
  shortRef = ref[0..7]
  if failed > 0
    puts "Warning! #{shortRef} has #{failed} red #{pluralize(failed, 'build', 'builds')} (plus #{successful} green and #{in_progress} in progress).\nDetails: #{uri}"
  elsif successful == 0
    puts "#{shortRef} hasn't built yet."
  else
    puts "#{shortRef} has #{successful} green #{pluralize(successful, 'build', 'builds')}."
  end

end
